#include "Item.h"


//inta function
Item::Item(std::string name, std::string serial, double price)
{
	this->_name = name;
	this->_serialNumber = serial;
	this->_unitPrice = price;
}

//delete function
Item::~Item()
{
	//nothing to delete
}
//get name
std::string Item::getName()
{
	return this->_name;
}
//returns serial number
std::string Item::getSerial()
{
	return this->_serialNumber;
}

int Item::getCount()
{
	return this->_count;
}

double Item::getPrice()
{
	return this->_unitPrice;
}

//shows total item price by count*price
double Item::totalPrice() const
{
	return this->_count*this->_unitPrice;
}

// compares the _serialNumber of those items.
bool Item::operator<(const Item& other) const
{
	return this->_serialNumber < other._serialNumber;
}

// compares the _serialNumber of those items.
bool Item::operator>(const Item& other) const
{
	return this->_serialNumber > other._serialNumber;
}

// compares the _serialNumber of those items.
bool Item::operator==(const Item& other) const
{
	return this->_serialNumber == other._serialNumber;
}









