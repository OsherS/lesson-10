#include"Customer.h"
#include<map>
#define listSize  10


int main()
{
	int choice = 0;
	int mini = 0; //mini choice
	int i = 0;
	std::string iname = "";
	std::string name = "";
	std::map<std::string, Customer> abcCustomers;
	Item itemList[10] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};


	while (choice != 4)
	{
		std::cout << "Welcome to MagshiMart! \n 1. to sign as customer and buy items \n 2. to uptade existing customer's items \n 3. to print the customer who pays the most \n 4. to exit \n";
		std::cin >> choice;
		if (choice == 1)
		{
			std::cout << "Enter name: ";
			std::cin >> name;
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				while (choice)
				{

					abcCustomers.insert({ name, Customer(name) });

					for (Item item : itemList)
					{
						std::cout << i + 1;
						std::cout << ". " << item.getName() << "\n";
						i++;
					}
					std::cout << "What do you want to buy? (exit - 0) ";
					std::cin >> mini;
					if (!mini)
					{
						choice = 0;
					}
					else
					{
						std::map<std::string, Customer>::iterator it = abcCustomers.find(name);
						if (it != abcCustomers.end())
						{
							it->second.addItem(itemList[mini - 1]);
						}
					}

					i = 0; //reset
				}
			}
			else
			{
				std::cout << "error\n";
				choice = 0;
			}
		}

		else if (choice == 2)
		{
			std::cout << "Enter name: ";
			std::cin >> name;
			if (abcCustomers.find(name) == abcCustomers.end())
			{
				std::cout << "Name doesn't exist. \n";
				choice = 0;
			}
			else
			{
				std::map<std::string, Customer>::iterator it = abcCustomers.find(name);
				if (it != abcCustomers.end())
				{
					for (Item item : it->second.getItems())
					{
						std::cout << item.getName() << "\n";
					}
				}
				std::cout << "\n1. Add items\n2. Remove items\n3. Back to menu\n";
				std::cin >> choice;
				if (choice == 1)
				{
					for (Item item : itemList)
					{
						std::cout << i + 1;
						std::cout << ". " << item.getName() << "\n";
						i++;
					}
					std::cout << "What do you want to buy? (exit - 0) ";
					std::cin >> mini;
					if (!mini)
					{
						choice = 0;
					}
					else
					{
						std::map<std::string, Customer>::iterator it = abcCustomers.find(name);
						if (it != abcCustomers.end())
						{
							it->second.addItem(itemList[mini - 1]);
						}
					}

					i = 0; //reset
				}
				else if (choice == 2)
				{
					while (choice)
					{
						std::cout << "\nWhat do you want to remove? enter item number\n";
						std::cin >> mini;
						std::map<std::string, Customer>::iterator it = abcCustomers.find(name);
						
						if (mini)
						{
							if (it != abcCustomers.end())
							{
								it->second.getItems().erase(itemList[mini - 1]);
							}
						}
						else
						{
							choice = 0;
						}
					}
				}
			}


		}

		else if (choice == 3)
		{
			double top = 0;
			std::map<std::string, Customer>::iterator it = abcCustomers.begin();
			while (it != abcCustomers.end())
			{
				if (it->second.totalSum() >= top)
				{
					top = it->second.totalSum();
					name = it->second.getName();
				}
			}
			std::cout << top << " - " << name;
		}
	}
	return 0;
}