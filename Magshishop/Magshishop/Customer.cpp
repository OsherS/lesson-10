#include "Customer.h"
//inta function
Customer::Customer(std::string name)
{
	this->_name = name;
}

//delete function
Customer::~Customer()
{
	//nothing to delete
}

//get name
std::string Customer::getName()
{
	return this->_name;
}

//get item set
std::set<Item> Customer::getItems()
{
	return this->_items;
}

//calculates total sum
double Customer::totalSum() const
{
	double counter = 0;
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		counter += it->totalPrice();
	}
	return counter;
}


//add item to the set
void Customer::addItem(Item item)
{
	this->_items.insert(item);
}
//remove item from the set
void Customer::removeItem(Item item)
{
	this->_items.erase(item);
}




